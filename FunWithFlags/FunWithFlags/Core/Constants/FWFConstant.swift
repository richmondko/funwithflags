//
//  FWFConstant.swift
//  FunWithFlags
//
//  Created by Richmond Ko on 7/8/21.
//

import Foundation

enum FWFConstant: String {
    case errorDomain = "com.richmondko.apps.funwithflags"
}
