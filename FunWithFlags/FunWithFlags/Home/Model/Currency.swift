//
//  Currency.swift
//  FunWithFlags
//
//  Created by Richmond Ko on 7/8/21.
//

import Foundation

struct Currency: Codable {
    let code: String?
    let name: String?
    let symbol: String?
}
